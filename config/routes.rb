Rails.application.routes.draw do
  resources :events, except: %i[show]
  resources :event_comments, only: %i[create]
  root "events#index"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end

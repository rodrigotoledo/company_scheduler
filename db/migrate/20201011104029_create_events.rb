class CreateEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :events do |t|
      t.string :company, null: false
      t.string :employee, null: false
      t.string :title, null: false
      t.text :description, null: false
      t.datetime :scheduled_at, null: false

      t.timestamps
    end
  end
end

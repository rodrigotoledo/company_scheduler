class ChangeEmployeeToAuthorInEvents < ActiveRecord::Migration[6.0]
  def change
    rename_column :events, :employee, :author
  end
end

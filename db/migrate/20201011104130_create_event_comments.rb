class CreateEventComments < ActiveRecord::Migration[6.0]
  def change
    create_table :event_comments do |t|
      t.references :event, null: false, foreign_key: true
      t.string :identified_by, null: false, default: "Anonymous"
      t.text :comment, null: false

      t.timestamps
    end
  end
end

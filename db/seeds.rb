require 'faker'
10.times do
  event = Event.create(
    company: Faker::Team.name,
    author: Faker::Name.name,
    title: Faker::Name.name_with_middle,
    description: Faker::Lorem.paragraph,
    scheduled_at: Faker::Date.forward(days: rand(10))
  )
  10.times do
    event.event_comments.build(
      identified_by: Faker::Name.name,
      comment: Faker::Lorem.paragraph
    )
    event.save
  end

end

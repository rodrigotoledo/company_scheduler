# == Schema Information
#
# Table name: event_comments
#
#  id            :integer          not null, primary key
#  event_id      :integer          not null
#  identified_by :string           default("Anonymous"), not null
#  comment       :text             not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class EventComment < ApplicationRecord
  default_scope { order(created_at: :desc) }
  belongs_to :event
end

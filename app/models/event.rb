# == Schema Information
#
# Table name: events
#
#  id           :integer          not null, primary key
#  company      :string           not null
#  author       :string           not null
#  title        :string           not null
#  description  :text             not null
#  scheduled_at :datetime         not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Event < ApplicationRecord
  default_scope { order(scheduled_at: :asc, created_at: :desc) }

  has_many :event_comments, dependent: :destroy
  has_one :event_comment, dependent: :destroy
end

class EventCommentsController < ApplicationController

  def create
    event_comment = EventComment.create(event_comment_params)
    redirect_to edit_event_path(event_comment.event_id)
  end

  private

    def event_comment_params
      params.require(:event_comment).permit(
        :event_id,
        :identified_by,
        :comment
      )
    end
end

class EventsController < ApplicationController
  before_action :set_events
  before_action :set_event, :only_if_exists, only: [:edit, :update, :destroy]

  def index
  end

  def new
    @event = Event.new
  end

  def create
    Event.create(event_params)
    redirect_to events_path
  end

  def edit
  end

  def update
    @event.update(event_params)
    redirect_to events_path
  end

  def destroy
    @event.destroy
    redirect_to events_path
  end

  private
    def set_event
      @event = Event.find(params[:id])
    end

    def only_if_exists
      redirect_to root_path unless @event.present?
    end

    def set_events
      @events = Event.order(scheduled_at: :desc)
    end

    def event_params
      params.require(:event).permit(
        :company,
        :author,
        :title,
        :description,
        :scheduled_at,
        event_comment_attributes: [:identified_by, :comment]
      )
    end
end

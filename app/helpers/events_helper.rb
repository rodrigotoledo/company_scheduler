module EventsHelper
  def event_comment_abbreviation(event_comment)
    if event_comment
      truncate "#{event_comment.identified_by}: #{event_comment.comment}"
    else
      '...'
    end
  end
end

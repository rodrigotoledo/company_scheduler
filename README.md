# Introduction

This application represents a Company Scheduler for employees

## Dependencies gem/js_libraries

- stimulus_reflex
- action cable
- cable_ready
- redis

## Starting

To basic application you will run basic commands to get results and open the application. Just run

``bundle``

``rails db:drop db:create db:migrate db:seed``

and access the http://localhost:3000

## Starting the real-time application

First you need have the redis installed in your development enviroment, in my case I use brew to install

``brew install redis``

after this start the service

``brew services start redis``

and if want stop the service

``brew services stop redis``
